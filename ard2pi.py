# Expects data in the format KEY:VALUE,KEY:VALUE;
# Will take as many values as you like.

import bluetooth # PyBluez
import urllib3
import requests
import time
import re
import sys

PUBLISH = False

# Thingsboard Settings
ACCESS_TOKEN = '' # Set to device access token acquired from Thingsboard
TB_SERVER = '' # Set to url for Thingsboard instance
URL = 'http://{SERVER}/api/v1/{ACCESS_TOKEN}/telemetry'.format(SERVER=TB_SERVER, ACCESS_TOKEN = ACCESS_TOKEN)

# HC05 Settings
MAC = '98:D3:31:F5:BA:B0' # Set to HC05 MAC
PORT = 1
SIZE = 1024

# Look at this sexy boy regex for making sure your data is correctly formatted
REGEX = r'^(\w+:[+-]?[0-9]+(\.[0-9]+)?(,(?!$)|$))+$'

# Connect to bluetooth device
def connectToDevice():
    connected = False
    while not connected:
        try:
            s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            s.connect((MAC,PORT))
            connected = True
        except bluetooth.btcommon.BluetoothError:
            print('Bluetooth device {} not found'.format(MAC))
        except KeyboardInterrupt:
            print('exiting script...')
            sys.exit(0)
    return s

# Formats data into a Thingsboard-compliant string
def formatData(d):
    # Removes any dirty spaces you might have in your data
    d = d.replace(' ','')

    # Only processes the data if it matches the pattern
    if re.match(REGEX, d):
        data = '{'
        d = d.split(',')
        for i in d:
            i = i.split(':')
            data +='"{}":{},'.format(i[0], i[1])
        data = data[:-1] + '}'
        return data
    return None

# Sends data to Thingsboard
def publishData(d):
    data = formatData(d)
    if data is None:
        print("Data format incorrect")
        return

    # If your data isn't formatted correctly for whatever reason, there's no
    # point sending it to TB
    try:
        r = requests.post(url=URL, data=data)
        print('{status}: {data}'.format(status=r.status_code, data=data))
    except requests.exceptions.ConnectionError:
        print('Connection error')

s = connectToDevice()

data = ''
while True:
    try:
        d = s.recv(SIZE)
        if d:
            out = d.decode('ASCII').strip()
            if ';' in out:
                out = out.strip().split(';')
                data += out[0]

                # To send data to TB, comment out this print statement and
                # uncomment the line below. Make sure you have an access token
                # from Thingsboard.
                if PUBLISH:
                    publishData(data)
                else:
                    dataString = formatData(data)
                    if dataString is not None:
                        print(dataString)
                    else:
                        print('Error')

                data = out[1]
            else:
                data += out
    except bluetooth.btcommon.BluetoothError:
        print('Bluetooth error, attempting to reconnect...')
        s = connectToDevice()
        pass
    except KeyboardInterrupt:
        s.close()
        print('exiting script...')
        break
